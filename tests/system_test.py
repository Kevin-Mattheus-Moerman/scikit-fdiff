#!/usr/bin/env python
# coding=utf-8

import string
from itertools import product

import numpy as np
import pytest
from sympy import Function, Idx, IndexedBase, Max, Min, Symbol, sympify

from skfdiff.core.spatial_schemes import FiniteDifferenceScheme
from skfdiff.core.system import Coordinate, PDEquation, Unknown, _build_sympy_namespace


@pytest.mark.parametrize("coord_name", ["x", "y", "z", "t", "xy", "u", "β"])
def test_coordinate(coord_name):
    if coord_name not in string.ascii_letters or len(coord_name) > 1:
        # only one character on ascii letters are accepted
        with pytest.raises(ValueError):
            coord = Coordinate(coord_name)
        return
    coord = Coordinate(coord_name)

    # indep var behaviour
    assert coord.name == coord_name
    assert coord.symbol == Symbol(coord_name)
    assert coord.discrete == IndexedBase(coord_name)
    assert coord.N == Symbol("N_%s" % coord_name, integer=True)
    assert coord.idx == Idx("%s_idx" % coord_name, coord.N)
    assert coord.step == Symbol("d%s" % coord_name)
    assert coord.step_value == (
        coord.discrete[coord.idx.upper] - coord.discrete[coord.idx.lower]
    ) / (coord.N - 1)
    assert coord.bound == (coord.idx.lower, coord.idx.upper)

    # should be hashable
    assert isinstance(hash(coord), int)


@pytest.mark.parametrize("coord_name", ["x", "y", "z", "t"])
def test_domain_coordinate(coord_name):
    coord = Coordinate(coord_name)
    assert coord.domain(5) == "bulk"
    assert coord.domain(1) == "bulk"
    assert coord.domain(coord.N - 2) == "bulk"
    assert coord.domain(coord.N) == "right"
    assert coord.domain(-1) == "left"

    assert coord.is_in_bulk(5)
    assert coord.is_in_bulk(1)
    assert coord.is_in_bulk(coord.N - 2)
    assert not coord.is_in_bulk(coord.N)
    assert not coord.is_in_bulk(-1)


@pytest.mark.parametrize("unk_", ["U", "V", "T", "ϕ"])
@pytest.mark.parametrize("coords", [tuple(), ("x",), ("x", "y")])
def test_dependent_variable(unk_, coords):
    unk_name = "%s%s" % (unk_, "(%s)" % ", ".join(coords) if coords else "")
    unk = Unknown(unk_name)
    coords = tuple([Coordinate(coord) for coord in coords])

    assert unk.name == unk_
    assert unk.coords == unk.coordinates
    assert unk.coords == coords

    assert unk.symbol == Function(unk_) if coords else Symbol(unk_)
    assert unk.discrete == IndexedBase(unk_) if coords else Symbol(unk_)
    assert unk.discrete_i == IndexedBase(unk_)[unk.c_idxs] if coords else Symbol(unk_)

    assert len(unk) == len(coords)

    assert unk.c_symbols == tuple([coord.symbol for coord in coords])
    assert unk.c_steps == tuple([coord.step for coord in coords])
    assert unk.c_step_values == tuple([coord.step_value for coord in coords])
    assert unk.c_discs == tuple([coord.discrete for coord in coords])
    assert unk.c_idxs == tuple([coord.idx for coord in coords])
    assert unk.c_Ns == tuple([coord.N for coord in coords])
    assert unk.c_bounds == tuple([coord.bound for coord in coords])
    assert unk.c_names == tuple([coord.name for coord in coords])

    # should be hashable
    assert isinstance(hash(unk), int)


def test_domain_dependent_variable():
    unk = Unknown("U(x, y)")

    N_x, N_y = unk.c_Ns

    assert unk.domains(-1, -1) == ("left", "left")
    assert unk.domains(-1, 0) == ("left", "left")
    assert unk.domains(3, -2) == ("bulk", "left")
    assert unk.domains(0, 0) == ("left", "left")
    assert unk.domains(5, 2) == ("bulk", "bulk")
    assert unk.domains(N_x, -2) == ("right", "left")
    assert unk.domains(5, N_y) == ("bulk", "right")
    assert unk.domains(N_x - 2, N_y - 2) == ("bulk", "bulk")
    assert unk.domains(N_x, N_y) == ("right", "right")

    assert not all(unk.is_in_bulk(-1, -1))
    assert not all(unk.is_in_bulk(-1, 0))
    assert not all(unk.is_in_bulk(3, -2))
    assert unk.is_in_bulk(0, 0)
    assert unk.is_in_bulk(5, 2)
    assert not all(unk.is_in_bulk(N_x, -2))
    assert not all(unk.is_in_bulk(5, N_y))
    assert unk.is_in_bulk(N_x - 1, N_y - 1)
    assert not all(unk.is_in_bulk(N_x, N_y))


def test_pde_equation_1D():
    x = Coordinate("x")
    U = Unknown("U(x)")

    pde = PDEquation("dxU", ["U(x)"])
    assert (
        pde.fdiff - (U.discrete[x.idx + 1] - U.discrete[x.idx - 1]) / (2 * x.step)
    ).expand() == 0

    pde = PDEquation(
        "dxU", ["U(x)"], schemes=(FiniteDifferenceScheme(scheme="left", accuracy=1),)
    )
    assert (
        pde.fdiff - (U.discrete[x.idx] - U.discrete[x.idx - 1]) / x.step
    ).expand() == 0

    pde = PDEquation(
        "dxU", ["U(x)"], schemes=(FiniteDifferenceScheme(scheme="right", accuracy=1),)
    )
    assert (
        pde.fdiff - (U.discrete[x.idx + 1] - U.discrete[x.idx]) / x.step
    ).expand() == 0


def test_pde_equation_2D():
    x = Coordinate("x")
    y = Coordinate("y")
    U = Unknown("U(x, y)")

    pde = PDEquation("dxxU + dyyU", ["U(x, y)"])

    fdiff = (
        (
            U.discrete[x.idx + 1, y.idx]
            - 2 * U.discrete[x.idx, y.idx]
            + U.discrete[x.idx - 1, y.idx]
        )
        / x.step ** 2
        + (
            U.discrete[x.idx, y.idx + 1]
            - 2 * U.discrete[x.idx, y.idx]
            + U.discrete[x.idx, y.idx - 1]
        )
        / y.step ** 2
    )
    assert (pde.fdiff - fdiff).expand() == 0


def test_build_sympy_namespace():

    x = Coordinate("x")
    y = Coordinate("y")
    U = Unknown("U(x, y)")

    ns = _build_sympy_namespace("dxxU + dyyU", (x, y), (U,), [])

    assert "x" in ns.keys()
    assert "y" in ns.keys()
    assert "dxxU" in ns.keys()
    assert "dyyU" in ns.keys()

    ns = _build_sympy_namespace("dxxU + dy(U + dyU + dxU)", (x, y), (U,), [])

    assert "x" in ns.keys()
    assert "y" in ns.keys()
    assert "dxU" in ns.keys()
    assert "dyU" in ns.keys()
    assert "dxxU" in ns.keys()
    assert "dy" in ns.keys()


# def test_apply_scheme():
#     x = Coordinate("x")
#     y = Coordinate("y")
#     U = Unknown("U(x, y)")
#     pde = PDEquation("dxxU + dyyU", ["U(x, y)"])

#     x_applied = _apply_centered_scheme(
#         2, x, U.symbol(x.symbol, y.symbol).diff(x.symbol, 2), 2, pde.symbolic_equation
#     )
#     y_applied = _apply_centered_scheme(
#         2, y, U.symbol(x.symbol, y.symbol).diff(y.symbol, 2), 2, pde.symbolic_equation
#     )

#     assert (
#         y_applied
#         - sympify(
#             "Derivative(U(x, y), (x, 2)) - 2*U(x, y)/dy**2 + "
#             "U(x, -dy + y)/dy**2 + U(x, dy + y)/dy**2"
#         )
#     ).expand() == 0

#     assert (
#         x_applied
#         - sympify(
#             "Derivative(U(x, y), (y, 2)) - 2*U(x, y)/dx**2 + "
#             "U(-dx + x, y)/dx**2 + U(dx + x, y)/dx**2"
#         )
#     ).expand() == 0

#     xy_applied = _apply_centered_scheme(
#         2, y, U.symbol(x.symbol, y.symbol).diff(y.symbol, 2), 2, x_applied
#     )
#     yx_applied = _apply_centered_scheme(
#         2, x, U.symbol(x.symbol, y.symbol).diff(x.symbol, 2), 2, y_applied
#     )
#     assert (xy_applied - yx_applied).expand() == 0


def test_upwind():
    x = Coordinate("x")
    U = Unknown("U(x)")

    c = Symbol("c")
    ap = Max(c, 0)
    am = Min(c, 0)

    pde = PDEquation("upwind(c, U, x, 1)", ["U(x)"], parameters=["c"])
    up = (U.discrete[x.idx + 1] - U.discrete[x.idx]) / x.step
    um = (U.discrete[x.idx] - U.discrete[x.idx - 1]) / x.step
    assert (ap * um + am * up - pde.fdiff).expand() == 0

    pde = PDEquation("upwind(c, U, x, 2)", ["U(x)"], parameters=["c"])
    up = (
        -U.discrete[x.idx + 2] + 4 * U.discrete[x.idx + 1] - 3 * U.discrete[x.idx]
    ) / (2 * x.step)
    um = (3 * U.discrete[x.idx] - 4 * U.discrete[x.idx - 1] + U.discrete[x.idx - 2]) / (
        2 * x.step
    )
    assert (ap * um + am * up - pde.fdiff).expand() == 0

    pde = PDEquation("upwind(c, U, x, 3)", ["U(x)"], parameters=["c"])
    up = (
        -U.discrete[x.idx + 2]
        + 6 * U.discrete[x.idx + 1]
        - 3 * U.discrete[x.idx]
        - 2 * U.discrete[x.idx - 1]
    ) / (6 * x.step)
    um = (
        2 * U.discrete[x.idx + 1]
        + 3 * U.discrete[x.idx]
        - 6 * U.discrete[x.idx - 1]
        + U.discrete[x.idx - 2]
    ) / (6 * x.step)
    assert (ap * um + am * up - pde.fdiff).expand() == 0


def test_pdesys():
    pass
