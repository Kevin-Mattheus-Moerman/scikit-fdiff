#!/usr/bin/env python
# coding=utf-8


import numpy as np
import pytest

from skfdiff import Model


@pytest.mark.parametrize("backend", ("numpy", "numba"))
def test_routines_api(backend):
    x = np.linspace(0, 10, 100)
    U = np.cos(x * 2 * np.pi / 10)
    model = Model(evolution_equations="dxxU", unknowns="U", backend=backend)
    fields = model.Fields(x=x, U=U)
    model.F(fields)
    model.J(fields)


def unknown_backend():
    with pytest.raises(KeyError):
        Model(evolution_equations="dxxU", unknowns="U", backend="foo")


if __name__ == "__main__":
    test_routines_api("numpy")
