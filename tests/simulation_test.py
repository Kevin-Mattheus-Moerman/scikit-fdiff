#!/usr/bin/env python
# -*- coding: utf-8 -*-


import numpy as np
import pytest

from skfdiff import Model, Simulation


@pytest.fixture
def heat_model():
    model = Model(
        evolution_equations="k * dxxT",
        unknowns="T",
        parameters="k",
        boundary_conditions="periodic",
    )
    return model


@pytest.mark.parametrize(
    "scheme", ["ROS2", "ROS3PRL", "ROS3PRw", "RODASPR", "Theta", "scipy_ivp"]
)
def test_simul_heat_eq(heat_model, scheme):
    x = np.linspace(0, 10, 50, endpoint=False)
    T = np.cos(x * 2 * np.pi / 10)
    initial_fields = heat_model.Fields(x=x, T=T, k=1)
    simul = Simulation(
        heat_model, initial_fields, scheme=scheme, dt=5, tmax=100, time_stepping=False
    )
    t, fields = simul.run()
    assert t == 100
    assert np.isclose(fields["T"].values.mean(), 0)


@pytest.mark.parametrize("scheme", ["ROS3PRL", "ROS3PRw", "RODASPR"])
def test_simul_heat_eq_dirichlet(scheme):
    x = np.linspace(0, 10, 50, endpoint=False)
    T = np.cos(x * 2 * np.pi / 10) + 1
    heat_model = Model(
        evolution_equations="k * dxxT",
        unknowns="T",
        parameters="k",
        boundary_conditions={("T", "x"): ("dirichlet", "dirichlet")},
    )
    initial_fields = heat_model.Fields(x=x, T=T, k=1)
    initial_fields["T"][0] = 1
    initial_fields["T"][-1] = 1

    simul = Simulation(
        heat_model, initial_fields, scheme=scheme, dt=5, tmax=100, time_stepping=False
    )

    t, fields = simul.run()
    assert np.isclose(t, 100)
    assert np.isclose(fields["T"], 1, atol=1e-1).all()


def test_simul_runtime_error(heat_model):
    x = np.linspace(0, 10, 50, endpoint=False)
    T = np.cos(x * 2 * np.pi / 10)
    initial_fields = heat_model.Fields(x=x, T=T, k=1)

    simul = Simulation(heat_model, initial_fields, dt=1, tol=1e-1, max_iter=2)
    with pytest.raises(RuntimeError):
        simul.run()

    simul = Simulation(heat_model, initial_fields, dt=1, tol=1e-1, dt_min=0.1)
    with pytest.raises(RuntimeError):
        simul.run()


def test_simul_repr(heat_model):
    x = np.linspace(0, 10, 50, endpoint=False)
    T = np.cos(x * 2 * np.pi / 10)
    initial_fields = heat_model.Fields(x=x, T=T, k=1)

    simul = Simulation(
        heat_model, initial_fields, dt=1, tol=1e-1, tmax=10, time_stepping=False
    )
    str(simul)
    str(simul.timer)


def test_simul_already_ended(heat_model):
    x = np.linspace(0, 10, 50, endpoint=False)
    T = np.cos(x * 2 * np.pi / 10)
    initial_fields = heat_model.Fields(x=x, T=T, k=1)

    simul = Simulation(
        heat_model, initial_fields, dt=1, tol=1e-1, tmax=10, time_stepping=False
    )
    simul.run()
    simul.run()


def test_simul_no_tmax(heat_model):
    x = np.linspace(0, 10, 50, endpoint=False)
    T = np.cos(x * 2 * np.pi / 10)
    initial_fields = heat_model.Fields(x=x, T=T, k=1)

    simul = Simulation(heat_model, initial_fields, dt=1, tol=1e-1, time_stepping=False)
    next(simul)


@pytest.mark.parametrize("progress", [True, False])
def test_simul_already_progress(heat_model, progress):
    x = np.linspace(0, 10, 50, endpoint=False)
    T = np.cos(x * 2 * np.pi / 10)
    initial_fields = heat_model.Fields(x=x, T=T, k=1)

    simul = Simulation(
        heat_model, initial_fields, dt=1, tol=1e-1, tmax=3, time_stepping=False
    )
    simul.run(progress=progress)
    assert simul.t == 3


def test_simul_pprocess(heat_model):
    x = np.linspace(0, 10, 50, endpoint=False)
    T = np.cos(x * 2 * np.pi / 10)
    initial_fields = heat_model.Fields(x=x, T=T, k=1)

    simul = Simulation(
        heat_model, initial_fields, dt=1, tol=1e-1, tmax=10, time_stepping=False
    )

    def compute_grad(simul):
        simul.fields["grad"] = ("x", np.gradient(simul.fields["T"].values))
        return simul

    simul.add_post_process("grad", compute_grad)
    simul.run()
    simul.remove_post_process("grad")
    assert simul.post_processes == []


if __name__ == "__main__":
    model = Model(
        evolution_equations="k * dxxT",
        unknowns="T",
        parameters="k",
        boundary_conditions="periodic",
    )
    test_simul_already_ended(model)
