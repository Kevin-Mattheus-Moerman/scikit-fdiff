---
title: 'scikit-finite-diff, a new tool for PDE solving'
tags:
  - python
  - pde
  - physical modelling
authors:
  - name: Nicolas Cellier
    orcid: 0000-0002-3759-3546	
    affiliation: 1
  - name: Christian Ruyer-Quil
    affiliation: 1
affiliations:
 - name: Université Savoie Mont-Blanc
   index: 1
date: 18 March 2019
bibliography: paper.bib
---

# Summary
Scikit-FDiff is a new tool for Partial Differential Equation (PDE) solving, written in pure Python, that focuses on reducing the time between the development of the mathematical model and the numerical solving.

It allows easy and automated finite difference discretization, thanks to symbolic processing (using the SymPy library [@meurer:2017]) which can deal with systems of multi-dimensional partial differential equations and complex boundary conditions.

Using finite differences, and the method of lines, Scikit-FDiff allows for the transformation of the original PDE into an Ordinary Differential Equation (ODE), providing fast computation of the temporal evolution vector and the Jacobian matrix. The latter is pre-computed in a symbolic way and is sparse by nature. An efficient vectorization allows one to formulate the numerical system in such a way as to facilitate the numerical solver work, even for complex multi-dimensional coupled cases. Systems can be evaluated with as few computational resources as possible, enabling the use of implicit and explicit solvers at a reasonable cost.

Scikit-FDiff stands out in comparison to other competitors, such as the FEniCS Project, Clawpack, or the Dedalus Project, in terms of its simplicity. Despite the fact that Scikit-FDiff uses a relatively simple method (finite-differences), which allows one to code in a way which is close to the mathematical model, it is able to solve real-world problems. It is however less suited than other packages for building specialized solvers.

Scikit-FDiff also contains several classic ODE solver implementations (some of which have been made available from dedicated python libraries), including the backward and forward Euler scheme, Crank-Nicolson, and explicit Runge-Kutta. More complex approaches, like the improved Rosenbrock-Wanner schemes [@rang:2015] up to the 6th order, are also available in Scikit-FDiff. The time-step is managed by a built-in error computation, which ensures the accuracy of the solution.

The main goal of this software is to minimize the time spent writing numerical solvers allowing one to focus on model development and data analysis. Therefore, Scikit-Fdiff has been formulated such that it can solve both simple examples and complex models in only a few line of code. In addition, extra tools are provided, such as data saving during the simulation, real-time plotting, and post-processing.

Scikit-FDiff can be used for a wide range of application including thermal diffusion, heterogeneous chemical reactor modeling, wave propagation and a large number of non-linear phenomena modeled as system of PDE. To date the authors have used it for solving heated falling-films, dropplet spread on windshield and simple moisture flow in a porous medium. Furthermore, Scikit-Fdiff has been validated for solving the shallow-water equation on dam-breaks [@Leveque:2002] (and the steady-lake case), and has been applied to heated falling-films [@Cellier:2018].

# Acknowledgements


# References
