How to cite
===========

Scikit-fdiff can be cited in two different DOI, with different purpose.

The first one is generated via `Zenodo <https://zenodo.org/>`_ and is linked to
a specific release. This DOI is useful to ensure the repeatability of your
research. An archive of that release is available for anyone using that DOI,
which guarantee that they will use the same tool as you, which should lead to
the same result.

The second one is a pending `JOSS <https://joss.theoj.org/>`_ publication (not
available yet, but hopefully soon) that is linked to the repository. This is
useful to cite scikit-fdiff as software, and is a kind way to thank the author
of that library and to recognise the usefulness of their works.

Both of them are available in the main page of the repository as well as in
the main page of the software.
